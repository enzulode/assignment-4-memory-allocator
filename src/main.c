#include "assert.h"
#include "mem.h"

#include <stdbool.h>
#include <stdio.h>

void test_ordinary_successful_memory_allocation();
void test_free_single_block_from_several_allocated();
void test_free_few_blocks_from_several_allocated();
void test_oom_new_memory_region_expands_the_old_one();
void test_oom_new_memory_region_does_not_expand_the_old_one();

int main()
{
    printf("Performing allocator tests...\n");

    test_ordinary_successful_memory_allocation();
    test_free_single_block_from_several_allocated();
    test_free_few_blocks_from_several_allocated();
    test_oom_new_memory_region_expands_the_old_one();
    test_oom_new_memory_region_does_not_expand_the_old_one();

    return 0;
}

void test_ordinary_successful_memory_allocation()
{
    printf("\nTest 'Ordinary memory allocation': ");

    void* heap = heap_init(128);
    assert(heap);

    void* allocated_block = _malloc(100);
    assert(allocated_block);

    _free(allocated_block);
    heap_term();

    printf("succeed\n");
}

void test_free_single_block_from_several_allocated()
{
    printf("\nTest 'Free single block from several blocks allocated': ");

    void* heap = heap_init(1024);
    assert(heap);

    void* block1 = _malloc(128);
    void* block2 = _malloc(128);
    void* block3 = _malloc(128);
    void* block4 = _malloc(128);

    assert(block1);
    assert(block2);
    assert(block3);
    assert(block4);

    _free(block2);
    
    assert(block1);
    assert(block3);
    assert(block4);

    heap_term();
    printf("succeed\n");
}

void test_free_few_blocks_from_several_allocated()
{
    printf("\nTest 'Free few blocks from several blocks allocated': ");

    void* heap = heap_init(1024);
    assert(heap);

    void* block1 = _malloc(128);
    void* block2 = _malloc(128);
    void* block3 = _malloc(128);
    void* block4 = _malloc(128);

    assert(block1);
    assert(block2);
    assert(block3);
    assert(block4);

    _free(block1);
    _free(block2);
    
    assert(block3);
    assert(block4);

    heap_term();
    printf("succeed\n");
}

void test_oom_new_memory_region_expands_the_old_one()
{
    printf("\nTest 'OOM - new memory region expands the old one': ");
    void* heap = heap_init(0);
    assert(heap);

    void* block1 = _malloc(512);
    void* block2 = _malloc(512);
    
    assert(block1);
    assert(block2);

    _free(block1);
    _free(block2);

    heap_term();
    printf("succeed\n");
}

void test_oom_new_memory_region_does_not_expand_the_old_one()
{
    printf("\nTest 'OOM - new memory region does not expand the old one': ");
    void* heap = heap_init(128);
    assert(heap);

    void* block1 = _malloc(1024 * 1024);
    void* block2 = _malloc(1024 * 1024);
    
    assert(block1);
    assert(block2);

    _free(block1);

    void* block3 = _malloc(1024 * 1024 * 1024);
    assert(block3);

    _free(block2);
    _free(block3);

    heap_term();
    printf("succeed\n");
}
